package ru.company.rssfeed.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ferfalk.simplesearchview.SimpleSearchView;
import com.prof.rssparser.Article;
import com.prof.rssparser.OnTaskCompleted;
import com.prof.rssparser.Parser;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.company.rssfeed.R;
import ru.company.rssfeed.adapter.FeedAdapter;
import ru.company.rssfeed.common.Utils;
import ru.company.rssfeed.decoration.SpaceDecoration;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
	
	private static final String TAG = MainActivity.class.getSimpleName();
	private String urlString = "https://www.androidauthority.com/feed";
	
	private final String KEY_URL = "URL";
	private final String KEY_SEARCH = "SEARCH";
	
	@BindView(R.id.main_toolbar)
	FrameLayout mToolbarContainer;
	
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	
	@BindView(R.id.searchView)
	SimpleSearchView mSearchView;
	
	@BindView(R.id.main_recycler_view)
	RecyclerView mRecyclerView;
	
	@BindView(R.id.main_edit_text)
	EditText mEditText;
	
	@BindView(R.id.main_load_btn)
	ImageButton mLoadButton;
	
	@BindView(R.id.main_card_search_url)
	CardView mCardView;
	
	private FeedAdapter mFeedAdapter;
	private Handler mHandler = new Handler(Looper.getMainLooper());
	private Parser mParser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		ButterKnife.bind(mToolbar, mToolbarContainer);
		ButterKnife.bind(mSearchView, mToolbarContainer);
		
		mParser = new Parser();
		mParser.onFinish(mOnTaskCompleted);
		
		// Установка заголовка.
		mToolbar.setTitle(getString(R.string.app_name));
		setSupportActionBar(mToolbar);
		
		mLoadButton.setOnClickListener(this);
		
		// Инициализация RecyclerView
		mRecyclerView.setHasFixedSize(true);
		RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(layoutManager);
		
		setRecyclerDecoration();
		
		mFeedAdapter = new FeedAdapter(this);
		mRecyclerView.setAdapter(mFeedAdapter);
		
		mSearchView.setOnQueryTextListener(mSearchViewListener);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putString(KEY_URL, mEditText.getText().toString());
		outState.putString(KEY_SEARCH, mSearchView.getSearchEditText().getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		String url = savedInstanceState.getString(KEY_URL);
		if (url != null && !url.isEmpty()) {
			mEditText.setText(url);
		}
		
		String search = savedInstanceState.getString(KEY_SEARCH);
		if (search != null && !search.isEmpty()) {
			mSearchView.showSearch(true);
			mSearchView.getSearchEditText().setText(search);
		}
	}
	
	private void setRecyclerDecoration() {
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mCardView.getLayoutParams();
		float padding = mCardView.getHeight() + mCardView.getContentPaddingBottom() +
				mCardView.getContentPaddingTop() +
				2 * layoutParams.topMargin + layoutParams.bottomMargin;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			padding += mCardView.getElevation();
		}
		
		mRecyclerView.addItemDecoration(new SpaceDecoration(
				this,
				SpaceDecoration.VERTICAL,
				(int) padding)
		);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		
		MenuItem item = menu.findItem(R.id.action_search);
		mSearchView.setMenuItem(item);
		
		return true;
	}
	
	@Override
	public void onBackPressed() {
		if (mSearchView.onBackPressed()) {
			return;
		}
		
		super.onBackPressed();
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.main_load_btn) {
			// Если интернет соединение присутствует,
			// загружаем новости.
			if (Utils.isNetworkAvailable(this)) {
				fetchFeed();
			} // Иначе сообщаем, что нет интернета.
			else {
				Toast.makeText(this,
						getString(R.string.check_network_connection),
						Toast.LENGTH_LONG
				).show();
			}
		}
	}
	
	public void fetchFeed() {
		String url = mEditText.getText().toString();
		if (url.isEmpty()) {
			url = urlString;
		}
		mParser.execute(url);
	}
	
	private OnTaskCompleted mOnTaskCompleted = new OnTaskCompleted() {
		@Override
		public void onTaskCompleted(@NotNull List<Article> list) {
			runOnUiThread(() -> mFeedAdapter.setItems(list));
		}
		
		@Override
		public void onError(@NotNull Exception e) {
			runOnUiThread(() -> {
				Log.d(TAG, e.getMessage());
				Toast.makeText(MainActivity.this,
						getString(R.string.loading_error),
						Toast.LENGTH_LONG
				).show();
			});
		}
	};
	
	private SimpleSearchView.OnQueryTextListener mSearchViewListener =
			new SimpleSearchView.OnQueryTextListener() {
		
		private static final String TAG = "SimpleSearchView";
		private String searchText;
		
		@Override
		public boolean onQueryTextSubmit(String query) {
			Log.d(TAG, "Submit:" + query);
			return false;
		}
		
		@Override
		public boolean onQueryTextChange(String newText) {
			searchText = newText;
			mHandler.removeCallbacks(mPostRunnable);
			mHandler.postDelayed(mPostRunnable, 500);
			return false;
		}
		
		@Override
		public boolean onQueryTextCleared() {
			Log.d(TAG, "Text cleared");
			return false;
		}
		
		private Runnable mPostRunnable = () -> {
			Log.d(TAG, "Text changed:" + searchText);
			mFeedAdapter.getFilter().filter(searchText);
		};
	};
}
