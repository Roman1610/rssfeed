package ru.company.rssfeed.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.company.rssfeed.R;
import ru.company.rssfeed.common.Constants;

import static android.provider.UserDictionary.Words.APP_ID;

public class DetailActivity extends AppCompatActivity {
	
	@BindView(R.id.detail_toolbar)
	Toolbar mToolbar;
	
	@BindView(R.id.detail_web_view)
	WebView mWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		ButterKnife.bind(this);
		
		String content = getIntent().getStringExtra(Constants.EXTRA_CONTENT);
		
		mToolbar.setTitle(getString(R.string.feed_detail));
		setSupportActionBar(mToolbar);
		
		final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_back);
		getSupportActionBar().setHomeAsUpIndicator(upArrow);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		
		mWebView.loadData(content, "text/html; charset=utf-8", "utf-8");
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
	
	@Override
	public boolean onSupportNavigateUp() {
		onBackPressed();
		return true;
	}
	
}
