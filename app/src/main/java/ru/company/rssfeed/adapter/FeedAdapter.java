package ru.company.rssfeed.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prof.rssparser.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.company.rssfeed.R;
import ru.company.rssfeed.common.Constants;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedHolder>
		implements Filterable {
	
	private Context mContext;
	
	private List<Article> mSourceArticles;
	private List<Article> mFilteredArticles;
	private FeedFilter mFeedFilter;
	
	public FeedAdapter(Context context) {
		mContext = context;
		mSourceArticles = Collections.emptyList();
		mFilteredArticles = Collections.emptyList();
		mFeedFilter = new FeedFilter();
	}
	
	@NonNull
	@Override
	public FeedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext)
				.inflate(R.layout.feed_item, parent, false);
		return new FeedHolder(view);
	}
	
	@Override
	public void onBindViewHolder(@NonNull FeedHolder holder, int position) {
		Article article = mFilteredArticles.get(holder.getAdapterPosition());
		holder.bind(article);
	}
	
	@Override
	public int getItemCount() {
		return mFilteredArticles.size();
	}
	
	public void setItems(List<Article> articles) {
		mSourceArticles = new ArrayList<>(articles);
		mFilteredArticles = new ArrayList<>(articles);
		notifyDataSetChanged();
	}
	
	@Override
	public Filter getFilter() {
		return mFeedFilter;
	}
	
	private class FeedFilter extends Filter {
		
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			String filterText = constraint.toString().toLowerCase();
			
			// Приводим текст к нижнему регистру.
			FilterResults filterResults = new FilterResults();
			if (constraint.length() > 0) {
				List<Article> tempList = new ArrayList<>();
				
				// Проходим по статьям и отбираем те где искомое словосочетание
				// присутствует в названии и в описаении.
				for (Article article : mSourceArticles) {
					if (article.getTitle() != null
							&& article.getTitle().toLowerCase().contains(filterText)) {
						tempList.add(article);
						continue;
					}
					if (article.getDescription() != null
							&& article.getDescription().toLowerCase().contains(filterText)) {
						tempList.add(article);
					}
				}
				
				filterResults.count = tempList.size();
				filterResults.values = tempList;
			} else {
				filterResults.count = mSourceArticles.size();
				filterResults.values = mSourceArticles;
			}
			
			return filterResults;
		}
		
		@Override
		@SuppressWarnings("unchecked")
		protected void publishResults(CharSequence constraint, FilterResults results) {
			mFilteredArticles = (List<Article>) results.values;
			notifyDataSetChanged();
		}
	}
	
	class FeedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		
		@BindView(R.id.feed_item_title_tv)
		TextView mTitleTv;
		@BindView(R.id.feed_item_description_tv)
		TextView mDescriptionTv;
		@BindView(R.id.feed_item_image_iv)
		ImageView mImageView;
		
		FeedHolder(@NonNull View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			itemView.setOnClickListener(this);
		}
		
		void bind(Article article) {
			mTitleTv.setText(article.getTitle());
			mDescriptionTv.setText(article.getDescription());
			
			if (article.getImage() != null && !article.getImage().isEmpty()) {
				Picasso.get()
						.load(article.getImage())
						.placeholder(R.drawable.image_placeholder)
						.error(R.drawable.image_error_placeholder)
						.into(mImageView);
			}
		}
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setAction(Constants.ACTION_RSS_CONTENT);
			intent.putExtra(Constants.EXTRA_CONTENT, mFilteredArticles.get(getAdapterPosition()).getContent());
			mContext.startActivity(intent);
		}
	}
}
