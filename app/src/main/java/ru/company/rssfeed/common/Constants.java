package ru.company.rssfeed.common;

final public class Constants {
	
	public static final String ACTION_RSS_CONTENT = "ru.company.rssfeed.SHOW_RSS_CONTENT";
	public static final String EXTRA_CONTENT = "rssContent";
	
}
